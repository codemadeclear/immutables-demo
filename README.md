#Immutables Demo

Demos and examples showing how to use the [Immutables library](http://immutables.github.io/).

For more details see the related posts: 

- [Introduction to the Immutables library](https://codemadeclear.com/index.php/2021/05/03/introduction-to-the-immutables-library/)
- [Immutables: how to customize the generated classes](https://codemadeclear.com/index.php/2021/05/31/immutables-how-to-customize-the-generated-classes/)
