package com.codemadeclear.immutables.demo.model;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

import static com.codemadeclear.immutables.demo.model.Employee.DEFAULT_NATIONALITY;
import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    public static final String FIRST_NAME = "John";
    public static final String LAST_NAME = "Doe";
    public static final LocalDate DATE_OF_BIRTH = LocalDate.of(1980, Month.APRIL, 21);

    @Test
    void generatedImmutableClassShouldHaveBuilder() {
        Employee e = ImmutableEmployee.builder()
                .firstName(FIRST_NAME)
                .lastName(LAST_NAME)
                .dateOfBirth(DATE_OF_BIRTH)
                .addSkills("programming", "accounting")
                .build();
        assertNotNull(e);
        assertNull(e.getId());
        assertEquals(FIRST_NAME, e.getFirstName());
        assertEquals(LAST_NAME, e.getLastName());
        assertEquals(DATE_OF_BIRTH, e.getDateOfBirth());
        assertEquals(DEFAULT_NATIONALITY, e.getNationality());
        List<String> skills = e.getSkills();
        assertNotNull(skills);
        assertSame(skills, e.getSkills()); //the actual list is returned, not a defensive copy
        assertFalse(skills.isEmpty());
        assertThrows(UnsupportedOperationException.class, () -> skills.add("something"));
        assertTrue(e.getFringeBenefits().isEmpty());
        assertFalse(e.getSalesTarget().isPresent());
    }
}
