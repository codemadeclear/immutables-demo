package com.codemadeclear.immutables.demo.model;

import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class AbstractTelephoneTest {

    public static final String COUNTRY_CODE = "+39";
    public static final String NUMBER = "06123456";
    public static final String LABEL = "home phone";

    @Test
    void shouldCreateTelephoneWithBuilder() {
        AbstractTelephone phone = TelephoneDto.builder()
                .countryCode(COUNTRY_CODE)
                .number(NUMBER)
                .label(LABEL)
                .build();
        assertNotNull(phone);
        assertEquals(COUNTRY_CODE, phone.getCountryCode());
        assertEquals(NUMBER, phone.getNumber());
        assertEquals(LABEL, phone.getLabel().orElse(null));
    }

    @Test
    void shouldCreateTelephoneWithStaticConstructor() {
        AbstractTelephone phone = TelephoneDto.of(COUNTRY_CODE, NUMBER, Optional.of(LABEL));
        assertNotNull(phone);
        assertEquals(COUNTRY_CODE, phone.getCountryCode());
        assertEquals(NUMBER, phone.getNumber());
        assertEquals(LABEL, phone.getLabel().orElse(null));
    }

    @Test
    void generatedClassShouldHaveToStringMethod() {
        AbstractTelephone phone = TelephoneDto.of(COUNTRY_CODE, NUMBER, Optional.of(LABEL));
        String toString = "" + phone;
        assertEquals(
                String.format("Telephone{countryCode=%s, number=%s, label=%s}",
                        COUNTRY_CODE,
                        NUMBER,
                        LABEL),
                toString);
    }

    @Test
    void generatedToStringShouldNotMentionNullFields() {
        AbstractTelephone phone = TelephoneDto.of(COUNTRY_CODE, NUMBER, Optional.empty());
        String toString = "" + phone;
        assertFalse(toString.contains("label"));
        assertEquals(
                String.format("Telephone{countryCode=%s, number=%s}",
                        COUNTRY_CODE,
                        NUMBER),
                toString);
    }

}
