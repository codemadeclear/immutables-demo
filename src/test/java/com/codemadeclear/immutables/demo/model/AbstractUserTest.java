package com.codemadeclear.immutables.demo.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AbstractUserTest {

    private static final String VALID_USERNAME = "user_num_1";
    private static final String VALID_PASSWORD = "password12345";
    private static final String USERNAME_INVALID_PATTERN = "username-";
    private static final String USERNAME_TOO_SHORT = "user1";
    private static final String USERNAME_TOO_LONG = "user_with_an_unreasonably_long_name";

    @Test
    void passwordShouldBeMasked() {
        AbstractUser u = User.builder()
                .username(VALID_USERNAME)
                .password(VALID_PASSWORD)
                .build();
        assertEquals("User{username=user_num_1, password=***HIDDEN***}", u.toString());
    }

    @Test
    void shouldThrowWhenUsernameHasInvalidPattern() {
        IllegalArgumentException e = assertThrows(
                IllegalArgumentException.class,
                () -> User.of(USERNAME_INVALID_PATTERN, VALID_PASSWORD));
        assertEquals("Username can only contain english letters, numbers and underscores",
                e.getMessage());
    }

    @Test
    void shouldThrowWhenUsernameIsTooShort() {
        IllegalArgumentException e = assertThrows(
                IllegalArgumentException.class,
                () -> User.of(USERNAME_TOO_SHORT, VALID_PASSWORD));
        assertEquals("Username length must be between 6 and 32",
                e.getMessage());
    }

    @Test
    void shouldThrowWhenUsernameIsTooLong() {
        IllegalArgumentException e = assertThrows(
                IllegalArgumentException.class,
                () -> User.of(USERNAME_TOO_LONG, VALID_PASSWORD));
        assertEquals("Username length must be between 6 and 32",
                e.getMessage());
    }
}
