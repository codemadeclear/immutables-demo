package com.codemadeclear.immutables.demo.model;

import org.immutables.value.Value;

@Value.Immutable
@Value.Style(typeImmutable = "*", redactedMask = "***HIDDEN***")
public abstract class AbstractUser {

    @Value.Parameter
    public abstract String getUsername();

    @Value.Parameter
    @Value.Redacted
    public abstract String getPassword();

    @Value.Check
    public void checkState() {
        assertUsernameHasValidPattern();
        assertUsernameHasValidLength();
    }

    private void assertUsernameHasValidPattern() {
        String username = getUsername();
        if (username == null) return;
        if (!username.matches("^[a-zA-Z0-9_]*$"))
            throw new IllegalArgumentException(
                    "Username can only contain english letters, numbers and underscores");
    }

    private void assertUsernameHasValidLength() {
        if (getUsername() == null) return;
        int length = getUsername().length();
        if (length < 6 || length > 32)
            throw new IllegalArgumentException("Username length must be between 6 and 32");
    }

}
