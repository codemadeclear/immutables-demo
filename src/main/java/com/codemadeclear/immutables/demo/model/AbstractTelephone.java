package com.codemadeclear.immutables.demo.model;

import org.immutables.value.Value;

import java.util.Optional;

@Value.Immutable
@Value.Style(typeAbstract = "Abstract*",
        typeImmutable = "*Dto",
        allParameters = true)
public abstract class AbstractTelephone {

    public abstract String getCountryCode();

    public abstract String getNumber();

    public abstract Optional<String> getLabel();

}
