package com.codemadeclear.immutables.demo.model;

import com.codemadeclear.immutables.demo.annotations.Nullable;
import org.immutables.value.Value;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Value.Immutable
public abstract class Employee {

    public static final String DEFAULT_NATIONALITY = "ITA";

    @Nullable
    public abstract Long getId();

    public abstract String getFirstName();

    public abstract String getLastName();

    public abstract LocalDate getDateOfBirth();

    public abstract List<String> getSkills();

    @Value.Default
    public String getNationality() {
        return DEFAULT_NATIONALITY;
    }

    public abstract Set<String> getFringeBenefits();

    public abstract Optional<Double> getSalesTarget();

}
